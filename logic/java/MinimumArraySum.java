import java.util.Scanner;

public class MinimumArraySum {
    public int getMinimum(int[] input) {
        /**
         * Please Write your code here, you are free to write any supporting function
         */
        return input.length;
    }

    public int[] getInput() {
        Scanner scanner = new Scanner(System.in);

        int count = scanner.nextInt();

        int[] input = new int[count];
        for (int i = 0;i < count;i++) {
            input[i] = scanner.nextInt();
        }

        return input;
    }

    public static void main(String[] arg) {
        Scanner scanner = new Scanner(System.in);

        MinimumArraySum minimum = new MinimumArraySum();
        System.out.println(minimum.getMinimum(minimum.getInput()));
    }
}
